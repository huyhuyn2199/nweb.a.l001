﻿using FA.JustBlog.Entity.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.Generic
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        protected readonly JustBlogDBContext Context;
        protected DbSet<TEntity> DbSet;
        public GenericRepository(JustBlogDBContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public void Create(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void CreateRange(List<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public void Delete(params object[] ids)
        {
            var entity = DbSet.Find(ids);
            if (entity == null)
                throw new ArgumentException($"{string.Join(";", ids)} not exist in the {typeof(TEntity).Name} table");
            DbSet.Remove(entity);
        }

        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            return DbSet.Where(predicate);
        }

        public TEntity FindId(int id)
        {
            return DbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet;
        }

        public TEntity GetById(params object[] primaryKey)
        {
            return DbSet.Find(primaryKey);
        }
        public IEnumerable<TEntity> GetPaging(IOrderedEnumerable<TEntity> orderBy, int currentPage = 1, int pageSize = 10, string filter = null)
        {
            throw new NotImplementedException();
        }
        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            DbSet.UpdateRange(entities);
        }
    }
}
