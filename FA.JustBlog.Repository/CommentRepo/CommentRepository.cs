﻿using FA.JustBlog.Entity.Context;
using FA.JustBlog.Entity.Entities;
using FA.JustBlog.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.CommentRepo
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        public CommentRepository(JustBlogDBContext context) : base(context)
        {
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            var maxId = Context.Comments.Max(x => x.Id);
            Comment comment = new Comment();
            comment.Id = maxId + 1;
            comment.Name = commentName;
            comment.CommentTime = DateTime.Now;
            comment.CommentText = commentBody;
            comment.CommentHeader = commentTitle;
            comment.Email = commentEmail;
            comment.PostId = postId;

            Context.Comments.Add(comment);
            Context.SaveChanges();
        }

        public IList<Comment> GetCommentsForPost(int postId)
        {
            var result = (from c in Context.Comments
                          join p in Context.Posts on c.PostId equals p.Id
                          where p.Id == postId
                          select c).ToList();
            return result;
        }

        public IList<Comment> GetCommentsForPost(Post post)
        {
            var result = (from c in Context.Comments
                          join p in Context.Posts on c.PostId equals p.Id
                          where c.PostId == post.Id
                          select c).ToList();
            return result;
        }
    }
}
