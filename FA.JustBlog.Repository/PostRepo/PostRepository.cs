﻿using FA.JustBlog.Entity.Context;
using FA.JustBlog.Entity.Entities;
using FA.JustBlog.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.PostRepo
{
    public class PostRepository : GenericRepository<Post>, IPostRepository
    {
        public PostRepository(JustBlogDBContext context) : base(context)
        {
        }

        public int CountPostsForCategory(string category)
        {
            var result = (from p in Context.Posts
                          join c in Context.Categories on p.CategoryId equals c.Id
                          where c.Name == category
                          select p).ToList();
            return result.Count();
        }

        public int CountPostsForTag(string tag)
        {
            var result = (from p in Context.Posts
                          join pt in Context.PostTagMaps on p.Id equals pt.PostId
                          join t in Context.Tags on pt.TagId equals t.Id
                          where t.Name == tag
                          select p).ToList();
            return result.Count();
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            var result = (from p in Context.Posts
                          where p.PostedOn.Year == year && p.PostedOn.Month == month && p.UrlSlug == urlSlug
                          select p).FirstOrDefault();
            return result;
        }

        public IList<Post> GetHighestPosts(int size)
        {
            var result = (from p in Context.Posts
                          orderby p.Rate descending
                          select p).Take(size);
            return result.ToList();
        }

        public IList<Post> GetLatestPost(int size)
        {
            var result = (from p in Context.Posts
                          orderby p.PostedOn descending
                          select p).Take(size);
            return result.ToList();
        }

        public IList<Post> GetMostViewedPost(int size)
        {
            var result = (from p in Context.Posts
                          orderby p.ViewCount descending
                          select p).Take(size);
            return result.ToList();
        }

        public IList<Post> GetPostsByCategory(string category)
        {
            var result = (from p in Context.Posts
                          join c in Context.Categories on p.CategoryId equals c.Id
                          where c.Name == category
                          select p).ToList();
            return result;
        }

        public IList<Post> GetPostsByMonth(DateTime monthYear)
        {
            var result = (from p in Context.Posts
                          where p.PostedOn.Month == monthYear.Month
                          select p).ToList();
            return result;
        }

        public IList<Post> GetPostsByTag(string tag)
        {
            var result = (from p in Context.Posts
                          join pt in Context.PostTagMaps on p.Id equals pt.PostId
                          join t in Context.Tags on pt.TagId equals t.Id
                          where t.Name == tag
                          select p).ToList();
            return result;
        }

        public IList<Post> GetPublisedPosts()
        {
            var result = (from p in Context.Posts
                          where p.Published == true
                          select p).ToList();
            return result;
        }

        public IList<Post> GetUnpublisedPosts()
        {
            var result = (from p in Context.Posts
                          where p.Published == false
                          select p).ToList();
            return result;
        }
    }
}
