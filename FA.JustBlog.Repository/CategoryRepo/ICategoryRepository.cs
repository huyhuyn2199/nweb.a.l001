﻿using FA.JustBlog.Entity.Entities;
using FA.JustBlog.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.CategoryRepo
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
