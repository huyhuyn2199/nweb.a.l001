﻿using FA.JustBlog.Entity.Context;
using FA.JustBlog.Entity.Entities;
using FA.JustBlog.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.TagRepo
{
    public class TagRepository : GenericRepository<Tag>, ITagRepository
    {
        public TagRepository(JustBlogDBContext context) : base(context)
        {
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            var result = (from t in Context.Tags
                          where t.UrlSlug == urlSlug
                          select t).FirstOrDefault();
            return result;
        }
    }
}
