﻿using FA.JustBlog.Entity.Context;
using FA.JustBlog.Repository.CategoryRepo;
using FA.JustBlog.Repository.CommentRepo;
using FA.JustBlog.Repository.PostRepo;
using FA.JustBlog.Repository.TagRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.Unit
{
    public interface IUnitOfWork : IDisposable
    {
        public ICategoryRepository CategoryRepository { get; }
        public IPostRepository PostRepository { get; }
        public ITagRepository TagRepository { get; }
        public ICommentRepository CommentRepository { get; }
        //public JustBlogDBContext justBlogDBContext { get;}
        int SaveChanges();
    }
}
