﻿using FA.JustBlog.Entity.Context;
using FA.JustBlog.Repository.CategoryRepo;
using FA.JustBlog.Repository.CommentRepo;
using FA.JustBlog.Repository.PostRepo;
using FA.JustBlog.Repository.TagRepo;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Repository.Unit
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JustBlogDBContext _dbContext;
        private ICategoryRepository _categoryRepository;
        private ICommentRepository _commentRepository;
        private IPostRepository _postRepository;
        private ITagRepository _tagRepository;
        public UnitOfWork(JustBlogDBContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public ICategoryRepository CategoryRepository => _categoryRepository ?? (_categoryRepository = new CategoryRepository(_dbContext));

        public IPostRepository PostRepository => _postRepository ?? (_postRepository = new PostRepository(_dbContext));

        public ITagRepository TagRepository => _tagRepository ?? (_tagRepository = new TagRepository(_dbContext));

        public ICommentRepository CommentRepository => _commentRepository ?? (_commentRepository = new CommentRepository(_dbContext));

        public JustBlogDBContext justBlogDBContext => _dbContext;

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }
    }
}
