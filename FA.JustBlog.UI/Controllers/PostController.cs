﻿using FA.JustBlog.Repository.Unit;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.UI.Controllers
{
    public class PostController : Controller
    {
        private readonly ILogger<PostController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public PostController(ILogger<PostController> logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            this._unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            var post = _unitOfWork.PostRepository.GetAll();
            return View(post);
        }
        public IActionResult Details(int id)
        {
            var post = _unitOfWork.PostRepository.FindId(id);
            return View(post);
        }
    }
}
