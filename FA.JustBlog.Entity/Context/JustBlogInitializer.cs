﻿using FA.JustBlog.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Context
{
    public static class JustBlogInitializer
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Âm nhạc", UrlSlug="am-nhac", Description="Tin tức về âm nhạc"},
                new Category { Id = 2, Name = "Phim ảnh", UrlSlug="phim-anh", Description="Tin tức về phim ảnh"},
                new Category { Id = 3, Name = "Thể thao", UrlSlug="the-thao", Description="Tin tức về thể thao"},
                new Category { Id = 4, Name = "Sức khỏe", UrlSlug="suc-khoe", Description="Tin tức về sức khỏe"},
                new Category { Id = 5, Name = "Kinh doanh", UrlSlug="kinh-doanh", Description="Tin tức về kinh doanh"}
                );
            modelBuilder.Entity<Tag>().HasData(
                new Tag { Id = 1, Name = "EPL", UrlSlug = "the-thao-npl", Description = "Bóng đá ngoại hạng Anh", Count = 1},
                new Tag { Id = 2, Name = "La Liga", UrlSlug = "the-thao-la-liga", Description = "Bóng đá Tây Ban Nha", Count = 2},
                new Tag { Id = 3, Name = "Serie A", UrlSlug = "the-thao-seria", Description = "Bóng đá Ý", Count = 3},
                new Tag { Id = 4, Name = "Big Mouth", UrlSlug = "phim-anh-big-mouth", Description = "Big Mouth là bộ phim truyền hình Hàn Quốc đang phát sóng với sự tham gia của Lee Jong-suk, Im Yoon-ah và Kim Joo-Hun.", Count = 4},
                new Tag { Id = 5, Name = "The Weekend", UrlSlug = "am-nhac-the-weekend", Description = "Sản phẩm âm nhạc mới \"Ngôi sao cô đơn\"", Count = 5}
                );
            modelBuilder.Entity<Post>().HasData(
                new Post
                {
                    Id = 1,
                    Title = "MU thua thảm & xếp bét bảng: Báo Anh giận dữ chỉ trích cả Ten Hag lẫn De Gea",
                    ShortDescription = "Báo giới Anh chỉ trích nặng nề cả HLV Ten Hag và thủ môn De Gea sau trận thua Brentford.",
                    PostContent = "Khi MU đánh bại Liverpool 4-0 trong trận giao hữu ở Bangkok, Thái Lan và giành thêm một số kết quả ấn tượng ở Australia, các fan MU tin rằng ngày phục hưng \"Quỷ đỏ\" đã ở rất gần. Nhưng hóa ra, đó chỉ là một \"cú lừa\"." +
                    "Sau 2 trận, vị trí của MU đang là thứ 20 với 0 điểm và hiệu số -5. Tờ The Sun khẳng định, tất cả đều trở nên nản lòng với tình cảnh hiện tại của \"Quỷ đỏ\", ngay cả khi mùa giải mới chỉ vừa bắt đầu. Ký giả Dan King khẳng định, " +
                    "MU giống như một căn bệnh trầm kha, đau đớn dai dẳng hết mùa này qua tháng khác.",
                    UrlSlug = "mu-thua-tham",
                    Published = true,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 3,
                    ViewCount = 1000,
                    RateCount = 600,
                    TotalRate = 900,
                    Rate = 1.5M
                },
                new Post
                {
                    Id = 2,
                    Title = "Tin mới nhất bóng đá trưa 14/8: Pep tin De Bruyne sẽ đe dọa cả Ngoại hạng Anh",
                    ShortDescription = "Bản tin bóng đá cập nhật trưa 14/8: HLV Pep Guardiola đã lên tiếng khẳng định tiền vệ Kevin De Bruyne sẽ còn thăng hoa hơn nữa để \"làm khổ\" phần còn lại của Ngoại hạng Anh.",
                    PostContent = "HLV Pep Guardiola đã phải thốt lên sau màn trình diễn của Kevin De Bruyne trước Bournemouth là \"không thể tin được\" và khẳng định tiền vệ người Bỉ hiện đang đạt phong độ cao nhất trong sự nghiệp. \"Tôi nghĩ màn trình diễn De Bruyne là không thể tin nổi. Kể từ mùa giải trước, cậu ấy đã cho thấy những tố chất đặc biệt. De Bruyne lúc này đang khao khát ghi bàn hơn bao giờ hết\", Pep Guardiola phát biểu." +
                    "Phong độ cao của De Bruyne đang là mối đe dọa lớn dành cho các đội bóng tại Ngoại hạng Anh, nhất là khi hàng công Man City đã được bổ sung tân binh Erling Haaland.",
                    UrlSlug = "ban-tin-bong-da",
                    Published = true,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 3,
                    ViewCount = 2000,
                    RateCount = 900,
                    TotalRate = 900,
                    Rate = 1.0M
                },
                new Post
                {
                    Id = 3,
                    Title = "Chuyển nhượng Big 6: Không phải Man City & Liverpool, đội nào mua quân \"ngon\" nhất?",
                    ShortDescription = "Nhóm Big 6 đã có hoạt động chuyển nhượng thế nào trong mùa hè 2022?",
                    PostContent = "Chúng ta đang bước vào những ngày giữa tháng 8 và với việc mùa giải Premier League đã đá được 1 vòng, thời điểm này thường các đội đã gần xong hết hoạt động mua bán cầu thủ. Nhóm Big Six cũng vậy và trong hè này họ đã cho thấy tham vọng của mình qua cách mua người."+
                    "Man City và Liverpool đều rước về 2 chân sút trẻ tiềm năng nhất châu lục là Erling Haaland và Darwin Nunez, trong khi Tottenham và Arsenal tậu quân ở một loạt vị trí để nâng cao chất lượng đội hình về mặt tổng quát. Chelsea và MU khởi đầu chậm nhưng vẫn có những bản hợp đồng đáng chú ý, từ Sterling và Koulibaly cho tới Eriksen và Lisandro Martinez."+
                    "Vậy cho đến hiện tại Big 6 của Premier League có hoạt động mua sắm tốt tới cỡ nào? Tờ The Athletic mới đây đã thực hiện một phân tích về dòng chảy cầu thủ của 6 ông lớn Ngoại hạng Anh để đưa ra đánh giá. Cuộc phân tích này được thực hiện dựa theo dữ liệu cung cấp bởi công ty TASPA chuyên về dữ liệu thể thao",
                    UrlSlug = "chuyen-nhuong-big-6",
                    Published = true,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 3,
                    ViewCount = 10000,
                    RateCount = 2450,
                    TotalRate = 5000,
                    Rate = 2.1M
                },
                new Post
                {
                    Id = 4,
                    Title = "'Big Mouth' tập 7: Big Mouse thực sự lộ diện, là người không ai ngờ tới - Yoona tiếp tục gặp nguy hiểm?",
                    ShortDescription = "Big Mouse thật có thể lộ diện ngay trong tập 7 'Big Mouth' và lại là người Park Chang Ho (Lee Jong Suk) không bao giờ ngờ tới.",
                    PostContent = "Trong tập 6 Big Mouth, Go Mi Ho (Yoona) đã vào trại giam Gucheon với tư cách tình nguyện viên và gặp Park Chang Ho (Lee Jong Suk). Anh khẳng định mình không phải Big Mouse và còn cho biết bản thân cùng hắn đang ngầm hợp tác. Hiện tại, 2 việc quan trọng nhất là tìm ra Big Mouse thật sự và nơi giấu luận án của cố giáo sư Seo."+
                    "Sau khi rời trại giam, Mi Ho dần tìm ra luận án của Seo Jae Young (Park Hoon) đang được giấu trong dây chuyền của Jang Hye Jin (Hong Ji Hee), vợ Han Jae Ho (Lee Yoo Joon). Cuối tập 5 tình thế trong trại giam Gucheon đảo ngược, Jung Chae Bong (Kim Jung Hyun) và Han Jae Ho (Lee Yoo Joon) đắc ý ra khỏi tù trong khi Park Chang Ho mất hết quyền lực. Nhưng tất cả có vẻ như chỉ nằm trong kế hoạch của luật sư Park..."+
                    "Mở đầu trailer tập 7, Han Jae Ho là kẻ duy nhất phải ở lại trại giam trong 3VIP tiếp tục bị tra tấn. Park Chang Ho đưa ra cảnh báo: 'Tôi sẽ bắt đầu đi săn'. Anh cũng trực tiếp nói chuyện với Han Jae Ho và hỏi: 'Trong tài liệu của giáo sư Seo có gì?'.",
                    UrlSlug = "big-mouth",
                    Published = true,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 2,
                    ViewCount = 30000,
                    RateCount = 12000,
                    TotalRate = 29500,
                    Rate = 2.5M
                },
                new Post
                {
                    Id = 5,
                    Title = "16 Typh hoạt động hết công suất khi vừa gia nhập SpaceSpeakers Label, nhận đặc quyền kết hợp cùng Touliver",
                    ShortDescription = "Chưa đầy 10 ngày kể từ khi 'Luật rừng' lên sóng, 16 Typh lại chuẩn bị phát hành sản phẩm solo mới.",
                    PostContent = "Mới đây, 16 Typh tung key visual chính thức cho MV Million Dollar Boy trên trang cá nhân. Nam rapper đeo mặt nạ, xuất hiện 'cực ngầu' trong khung cảnh bí ẩn được bao quanh bởi những đóa hoa hồng sáng rực khung hình. Dòng chữ 'Million Dollar Boy' được đính đá cầu kỳ, xuất hiện rải rác các đốm sáng, thể hiện cho sự sang trọng, giàu có. Đây được dự đoán là MV đậm mùi tiền và sự chất chơi của 16 Typh."+
                    "Trước đó, đoạn teaser dài 20 giây của MV Million Dollar Boy trình làng vào tối 11/8, gây chú ý trên cộng đồng mạng vì hình ảnh bí hiểm, đi kèm lyrics có nhắc đến tên Binz và Touliver. Đó là các câu: 'Binz nói với anh / Million dollar flow / Thành công đang tới nhanh / Million dollar flow / Touliver making beat / Million dollar flow / You know 16 Typh'.",
                    UrlSlug = "16-Typh",
                    Published = true,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 1,
                    ViewCount = 1000,
                    RateCount = 600,
                    TotalRate = 900,
                    Rate = 1.5M
                },
                new Post
                {
                    Id = 6,
                    Title = "Chi Pu khoe vũ đạo cực đỉnh trong bản Dance 'Black Hickey'",
                    ShortDescription = "Ngay khi vừa ra mắt bản Dance 'Black Hickey', Chi Pu đã nhận được nhiều bình luận khen ngợi của cộng đồng mạng.",
                    PostContent = "Tối 12/8, Chi Pu đã tung phiên bản Dance cho MV Black Hickey. Trên nền nhạc bắt tai, cực cuốn, Chi Pu và vũ đoàn Bước nhảy khiến người xem mãn nhãn với những động tác vũ đạo điêu luyện.",
                    UrlSlug = "Chipu",
                    Published = false,
                    PostedOn = DateTime.Parse("2022-08-14"),
                    Modified = false,
                    CategoryId = 1,
                    ViewCount = 1000,
                    RateCount = 600,
                    TotalRate = 900,
                    Rate = 1.5M
                }
                );
            modelBuilder.Entity<PostTagMap>().HasData(
                new PostTagMap { PostId = 1, TagId = 1 },
                new PostTagMap { PostId = 2, TagId = 1 },
                new PostTagMap { PostId = 3, TagId = 1 },
                new PostTagMap { PostId = 4, TagId = 4 },
                new PostTagMap { PostId = 5, TagId = 5 },
                new PostTagMap { PostId = 6, TagId = 5 }
                );
            modelBuilder.Entity<Comment>().HasData(
                new Comment { Id = 1,
                    Name = "Huy",
                    Email = "HuyHG2199",
                    CommentHeader = "Hóng",
                    CommentText = "Không biết ai là Big Mouth",
                    CommentTime = DateTime.Now,
                    PostId = 4 },
                new Comment
                {
                    Id = 2,
                    Name = "Huyen",
                    Email = "HuyenDT43",
                    CommentHeader = "Hồi hộp ghê",
                    CommentText = "Nghi ông có hình xăm là Big Mouth",
                    CommentTime = DateTime.Now,
                    PostId = 4
                },
                new Comment
                {
                    Id = 3,
                    Name = "Muối",
                    Email = "MuoiMuoi",
                    CommentHeader = "Tệ",
                    CommentText = "MU ngày càng xuống dốc",
                    CommentTime = DateTime.Now,
                    PostId = 1
                }
                );
        }
    }
}
