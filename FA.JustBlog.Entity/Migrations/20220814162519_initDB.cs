﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FA.JustBlog.Entity.Migrations
{
    public partial class initDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Post",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ShortDescription = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    PostContent = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    PostedOn = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2022, 8, 14, 23, 25, 18, 922, DateTimeKind.Local).AddTicks(2200)),
                    Modified = table.Column<bool>(type: "bit", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    ViewCount = table.Column<int>(type: "int", nullable: false),
                    RateCount = table.Column<int>(type: "int", nullable: false),
                    TotalRate = table.Column<int>(type: "int", nullable: false),
                    Rate = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Post", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Post_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CommentHeader = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    CommentText = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    CommentTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2022, 8, 14, 23, 25, 18, 921, DateTimeKind.Local).AddTicks(9191)),
                    PostId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_Post_PostId",
                        column: x => x.PostId,
                        principalTable: "Post",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTagMap",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    TagId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTagMap", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_PostTagMap_Post_PostId",
                        column: x => x.PostId,
                        principalTable: "Post",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTagMap_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "Id", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { 1, "Tin tức về âm nhạc", "Âm nhạc", "am-nhac" },
                    { 2, "Tin tức về phim ảnh", "Phim ảnh", "phim-anh" },
                    { 3, "Tin tức về thể thao", "Thể thao", "the-thao" },
                    { 4, "Tin tức về sức khỏe", "Sức khỏe", "suc-khoe" },
                    { 5, "Tin tức về kinh doanh", "Kinh doanh", "kinh-doanh" }
                });

            migrationBuilder.InsertData(
                table: "Tag",
                columns: new[] { "Id", "Count", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { 1, 1, "Bóng đá ngoại hạng Anh", "EPL", "the-thao-npl" },
                    { 2, 2, "Bóng đá Tây Ban Nha", "La Liga", "the-thao-la-liga" },
                    { 3, 3, "Bóng đá Ý", "Serie A", "the-thao-seria" },
                    { 4, 4, "Big Mouth là bộ phim truyền hình Hàn Quốc đang phát sóng với sự tham gia của Lee Jong-suk, Im Yoon-ah và Kim Joo-Hun.", "Big Mouth", "phim-anh-big-mouth" },
                    { 5, 5, "Sản phẩm âm nhạc mới \"Ngôi sao cô đơn\"", "The Weekend", "am-nhac-the-weekend" }
                });

            migrationBuilder.InsertData(
                table: "Post",
                columns: new[] { "Id", "CategoryId", "Modified", "PostContent", "PostedOn", "Published", "Rate", "RateCount", "ShortDescription", "Title", "TotalRate", "UrlSlug", "ViewCount" },
                values: new object[,]
                {
                    { 1, 3, false, "Khi MU đánh bại Liverpool 4-0 trong trận giao hữu ở Bangkok, Thái Lan và giành thêm một số kết quả ấn tượng ở Australia, các fan MU tin rằng ngày phục hưng \"Quỷ đỏ\" đã ở rất gần. Nhưng hóa ra, đó chỉ là một \"cú lừa\".Sau 2 trận, vị trí của MU đang là thứ 20 với 0 điểm và hiệu số -5. Tờ The Sun khẳng định, tất cả đều trở nên nản lòng với tình cảnh hiện tại của \"Quỷ đỏ\", ngay cả khi mùa giải mới chỉ vừa bắt đầu. Ký giả Dan King khẳng định, MU giống như một căn bệnh trầm kha, đau đớn dai dẳng hết mùa này qua tháng khác.", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1.5m, 600, "Báo giới Anh chỉ trích nặng nề cả HLV Ten Hag và thủ môn De Gea sau trận thua Brentford.", "MU thua thảm & xếp bét bảng: Báo Anh giận dữ chỉ trích cả Ten Hag lẫn De Gea", 900, "mu-thua-tham", 1000 },
                    { 2, 3, false, "HLV Pep Guardiola đã phải thốt lên sau màn trình diễn của Kevin De Bruyne trước Bournemouth là \"không thể tin được\" và khẳng định tiền vệ người Bỉ hiện đang đạt phong độ cao nhất trong sự nghiệp. \"Tôi nghĩ màn trình diễn De Bruyne là không thể tin nổi. Kể từ mùa giải trước, cậu ấy đã cho thấy những tố chất đặc biệt. De Bruyne lúc này đang khao khát ghi bàn hơn bao giờ hết\", Pep Guardiola phát biểu.Phong độ cao của De Bruyne đang là mối đe dọa lớn dành cho các đội bóng tại Ngoại hạng Anh, nhất là khi hàng công Man City đã được bổ sung tân binh Erling Haaland.", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1.0m, 900, "Bản tin bóng đá cập nhật trưa 14/8: HLV Pep Guardiola đã lên tiếng khẳng định tiền vệ Kevin De Bruyne sẽ còn thăng hoa hơn nữa để \"làm khổ\" phần còn lại của Ngoại hạng Anh.", "Tin mới nhất bóng đá trưa 14/8: Pep tin De Bruyne sẽ đe dọa cả Ngoại hạng Anh", 900, "ban-tin-bong-da", 2000 },
                    { 3, 3, false, "Chúng ta đang bước vào những ngày giữa tháng 8 và với việc mùa giải Premier League đã đá được 1 vòng, thời điểm này thường các đội đã gần xong hết hoạt động mua bán cầu thủ. Nhóm Big Six cũng vậy và trong hè này họ đã cho thấy tham vọng của mình qua cách mua người.Man City và Liverpool đều rước về 2 chân sút trẻ tiềm năng nhất châu lục là Erling Haaland và Darwin Nunez, trong khi Tottenham và Arsenal tậu quân ở một loạt vị trí để nâng cao chất lượng đội hình về mặt tổng quát. Chelsea và MU khởi đầu chậm nhưng vẫn có những bản hợp đồng đáng chú ý, từ Sterling và Koulibaly cho tới Eriksen và Lisandro Martinez.Vậy cho đến hiện tại Big 6 của Premier League có hoạt động mua sắm tốt tới cỡ nào? Tờ The Athletic mới đây đã thực hiện một phân tích về dòng chảy cầu thủ của 6 ông lớn Ngoại hạng Anh để đưa ra đánh giá. Cuộc phân tích này được thực hiện dựa theo dữ liệu cung cấp bởi công ty TASPA chuyên về dữ liệu thể thao", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 2.1m, 2450, "Nhóm Big 6 đã có hoạt động chuyển nhượng thế nào trong mùa hè 2022?", "Chuyển nhượng Big 6: Không phải Man City & Liverpool, đội nào mua quân \"ngon\" nhất?", 5000, "chuyen-nhuong-big-6", 10000 },
                    { 4, 2, false, "Trong tập 6 Big Mouth, Go Mi Ho (Yoona) đã vào trại giam Gucheon với tư cách tình nguyện viên và gặp Park Chang Ho (Lee Jong Suk). Anh khẳng định mình không phải Big Mouse và còn cho biết bản thân cùng hắn đang ngầm hợp tác. Hiện tại, 2 việc quan trọng nhất là tìm ra Big Mouse thật sự và nơi giấu luận án của cố giáo sư Seo.Sau khi rời trại giam, Mi Ho dần tìm ra luận án của Seo Jae Young (Park Hoon) đang được giấu trong dây chuyền của Jang Hye Jin (Hong Ji Hee), vợ Han Jae Ho (Lee Yoo Joon). Cuối tập 5 tình thế trong trại giam Gucheon đảo ngược, Jung Chae Bong (Kim Jung Hyun) và Han Jae Ho (Lee Yoo Joon) đắc ý ra khỏi tù trong khi Park Chang Ho mất hết quyền lực. Nhưng tất cả có vẻ như chỉ nằm trong kế hoạch của luật sư Park...Mở đầu trailer tập 7, Han Jae Ho là kẻ duy nhất phải ở lại trại giam trong 3VIP tiếp tục bị tra tấn. Park Chang Ho đưa ra cảnh báo: 'Tôi sẽ bắt đầu đi săn'. Anh cũng trực tiếp nói chuyện với Han Jae Ho và hỏi: 'Trong tài liệu của giáo sư Seo có gì?'.", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 2.5m, 12000, "Big Mouse thật có thể lộ diện ngay trong tập 7 'Big Mouth' và lại là người Park Chang Ho (Lee Jong Suk) không bao giờ ngờ tới.", "'Big Mouth' tập 7: Big Mouse thực sự lộ diện, là người không ai ngờ tới - Yoona tiếp tục gặp nguy hiểm?", 29500, "big-mouth", 30000 },
                    { 5, 1, false, "Mới đây, 16 Typh tung key visual chính thức cho MV Million Dollar Boy trên trang cá nhân. Nam rapper đeo mặt nạ, xuất hiện 'cực ngầu' trong khung cảnh bí ẩn được bao quanh bởi những đóa hoa hồng sáng rực khung hình. Dòng chữ 'Million Dollar Boy' được đính đá cầu kỳ, xuất hiện rải rác các đốm sáng, thể hiện cho sự sang trọng, giàu có. Đây được dự đoán là MV đậm mùi tiền và sự chất chơi của 16 Typh.Trước đó, đoạn teaser dài 20 giây của MV Million Dollar Boy trình làng vào tối 11/8, gây chú ý trên cộng đồng mạng vì hình ảnh bí hiểm, đi kèm lyrics có nhắc đến tên Binz và Touliver. Đó là các câu: 'Binz nói với anh / Million dollar flow / Thành công đang tới nhanh / Million dollar flow / Touliver making beat / Million dollar flow / You know 16 Typh'.", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1.5m, 600, "Chưa đầy 10 ngày kể từ khi 'Luật rừng' lên sóng, 16 Typh lại chuẩn bị phát hành sản phẩm solo mới.", "16 Typh hoạt động hết công suất khi vừa gia nhập SpaceSpeakers Label, nhận đặc quyền kết hợp cùng Touliver", 900, "16-Typh", 1000 },
                    { 6, 1, false, "Tối 12/8, Chi Pu đã tung phiên bản Dance cho MV Black Hickey. Trên nền nhạc bắt tai, cực cuốn, Chi Pu và vũ đoàn Bước nhảy khiến người xem mãn nhãn với những động tác vũ đạo điêu luyện.", new DateTime(2022, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 1.5m, 600, "Ngay khi vừa ra mắt bản Dance 'Black Hickey', Chi Pu đã nhận được nhiều bình luận khen ngợi của cộng đồng mạng.", "Chi Pu khoe vũ đạo cực đỉnh trong bản Dance 'Black Hickey'", 900, "Chipu", 1000 }
                });

            migrationBuilder.InsertData(
                table: "Comment",
                columns: new[] { "Id", "CommentHeader", "CommentText", "CommentTime", "Email", "Name", "PostId" },
                values: new object[,]
                {
                    { 1, "Hóng", "Không biết ai là Big Mouth", new DateTime(2022, 8, 14, 23, 25, 18, 921, DateTimeKind.Local).AddTicks(5532), "HuyHG2199", "Huy", 4 },
                    { 2, "Hồi hộp ghê", "Nghi ông có hình xăm là Big Mouth", new DateTime(2022, 8, 14, 23, 25, 18, 921, DateTimeKind.Local).AddTicks(5544), "HuyenDT43", "Huyen", 4 },
                    { 3, "Tệ", "MU ngày càng xuống dốc", new DateTime(2022, 8, 14, 23, 25, 18, 921, DateTimeKind.Local).AddTicks(5545), "MuoiMuoi", "Muối", 1 }
                });

            migrationBuilder.InsertData(
                table: "PostTagMap",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 4, 4 },
                    { 5, 5 },
                    { 6, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_PostId",
                table: "Comment",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_CategoryId",
                table: "Post",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTagMap_TagId",
                table: "PostTagMap",
                column: "TagId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "PostTagMap");

            migrationBuilder.DropTable(
                name: "Post");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "Category");
        }
    }
}
