﻿using FA.JustBlog.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Config
{
    public class TagConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.ToTable("Tag");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Id).ValueGeneratedOnAdd();
            builder.Property(t => t.Name).IsRequired().HasMaxLength(50).IsUnicode(true);
            builder.Property(t => t.UrlSlug).IsRequired().HasMaxLength(50);
            builder.Property(t => t.Description).HasMaxLength(1024).IsUnicode(true);
            builder.Property(t => t.Count).IsRequired();
        }
    }
}
