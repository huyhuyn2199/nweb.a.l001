﻿using FA.JustBlog.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Config
{
    public class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable("Comment");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.Name).IsRequired().HasMaxLength(50).IsUnicode(true);
            builder.Property(c => c.Email).IsRequired().HasMaxLength(50);
            builder.Property(c => c.CommentHeader).IsRequired().HasMaxLength(255).IsUnicode(true);
            builder.Property(c => c.CommentText).IsRequired().HasMaxLength(1024).IsUnicode(true);
            builder.Property(c => c.CommentTime).IsRequired().HasDefaultValue(DateTime.Now);
            builder.HasOne(p => p.Post)
                .WithMany(c => c.Comments)
                .HasForeignKey(c => c.PostId);
        }
    }
}
