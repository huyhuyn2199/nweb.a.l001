﻿using FA.JustBlog.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Config
{
    public class PostConfig : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Title).IsRequired().HasMaxLength(255).IsUnicode(true);
            builder.Property(p => p.ShortDescription).HasMaxLength(255).IsUnicode(true);
            builder.Property(p => p.PostContent).IsRequired().HasMaxLength(1024).IsUnicode(true);
            builder.Property(p => p.UrlSlug).IsRequired().HasMaxLength(50);
            builder.Property(p => p.PostedOn).IsRequired().HasDefaultValue(DateTime.Now);
            builder.Property(p => p.ViewCount).IsRequired();
            builder.Property(p => p.RateCount).IsRequired();
            builder.Property(p => p.TotalRate).IsRequired();
            builder.HasOne(c => c.Category)
                .WithMany(p => p.Posts)
                .HasForeignKey(p => p.CategoryId);
        }
    }
}
