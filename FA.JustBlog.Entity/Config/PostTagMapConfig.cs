﻿using FA.JustBlog.Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Entity.Config
{
    public class PostTagMapConfig : IEntityTypeConfiguration<PostTagMap>
    {
        public void Configure(EntityTypeBuilder<PostTagMap> builder)
        {
            builder.ToTable("PostTagMap");
            builder.HasKey(x => new { x.PostId, x.TagId });
            builder.HasOne(p => p.Post)
                .WithMany(x => x.PostTagMaps)
                .HasForeignKey(x => x.PostId);
            builder.HasOne(t => t.Tag)
                .WithMany(x => x.PostTagMaps)
                .HasForeignKey(x => x.TagId);
        }
    }
}
